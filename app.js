const colorMessage = require('./config/errors-message').colors;
const argv = require('./config/yargs').argv;
const { crearArchivo, listarTabla } = require('./multiplicar/multiplicar');


//  console.log(argv);

switch (argv._[0].toLocaleLowerCase()) {
  case 'listar':
    console.log('Listar');
    listarTabla(argv.base, argv.limite).then(listar => listar);
    break;

  case 'crear':
    console.log('Crear');

    crearArchivo(argv.base, argv.limite).then(nombreArchivo => {
      console.log(`El nombre del archivo creado:`, colorMessage.info(nombreArchivo));
    }).catch(err => console.log(err));

    //listarTabla(argv.base, argv.limite).then(listar => listar);
    break;

  default:
    console.log('Comando no reconocido..!!');
}


// console.log(argv2);

// let parametro = argv[2];
// let base = parametro.split('=')[1];

// crearArchivo(base).then(nombreArchivo => {
//   console.log(`El nombre del archivo creado: ${nombreArchivo}`);
// }).catch(err => console.log(err))



