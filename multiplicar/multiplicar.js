const colors = require('colors');
const fs = require('fs');

let listarTabla = async (base, limite = 10)=>{

console.log("=====================================".green);
console.log(`==========Tabla de ${base}===========`.red);
console.log("=====================================".blue);

  for(let i = 1; i <= limite ; i++){   
    console.log( `${base} * ${i} = ${base * i}\n`);
}
         
}

let crearArchivo = async(base, limite = 10) =>{

  if(!Number(base)){
    throw new Error('El valor introducido para la base no es un número..!!')
  } 

  let data = '';

  for(let i = 1; i <= limite ; i++){
    data += `${base} * ${i} = ${base * i}\n`;
}

  fs.writeFile(`tablas/tabla-${base}.txt`, data, (err) => {
    if (err) throw err;
    console.log(`El archivo tabla-${base} ha sido creado!`);
  });

  return `tabla-${base}.txt`;
 }

 module.exports ={
   crearArchivo,
   listarTabla
 }
