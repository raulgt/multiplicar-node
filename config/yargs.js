const opts = {
    base: {
        demand: true,
        alias: 'b'
    },
    limite: {
        alias: 'l',
        default: 10
    },
    size: {
        alias: 's',
        choices: ['xs', 's', 'm']
    }
}

const opts2 = {
    base: {
        demand: true,
        alias: 'b'
    },
    limite: {
        alias: 'l',
        default: 10
    }
}


const argv = require('yargs')
    .command('listar', 'Imprime en consola la tabla de multiplicar', opts)
    .command('crear', 'Crear la tabla de multiplicar', opts2).help()
    .argv;

module.exports = {
    argv
}